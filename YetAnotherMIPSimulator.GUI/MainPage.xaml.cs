﻿using System.Diagnostics;
using YetAnotherMIPSimulator.MIPS;
using YetAnotherMIPSimulator.MIPS.Config;
using YetAnotherMIPSimulator.MIPS.Parsers;

namespace YetAnotherMIPSimulator.GUI;

[QueryProperty(nameof(Config), "config")]
public partial class MainPage : ContentPage
{
	private FileResult CodeFile { get; set; }
	private MIPSConfig _mipsConfig;

	public MIPSConfig Config 
	{ 
		get => _mipsConfig;
		set => _mipsConfig = value;
	}

	public MainPage()
	{
		InitializeComponent();

		_mipsConfig = new MIPSConfig
		{
			RegisterNumber = 32,
			MaxDataMemSize = 100,
			MaxInstructionMemSize = 100
		};
	}

    private async void Pick_File(object sender, EventArgs e)
    {

		var result = await FilePicker.Default.PickAsync(new PickOptions
		{
			FileTypes = new FilePickerFileType(
				new Dictionary<DevicePlatform, IEnumerable<string>>
				{
					{ DevicePlatform.WinUI, new [] { ".txt" } }
				}
			)
		});

		if (result is null) return;

		CodeFile = result;

        SelectedFile.Text = CodeFile.FileName;
        
    }

    private async void Open_MIPS_Config(object sender, EventArgs e)
    {
		await Shell.Current.GoToAsync("MIPS-configuration", 
			new Dictionary<string, object>
			{
				{ "config", _mipsConfig }
			});
    }

    private async void Execute(object sender, EventArgs e)
    {
		if (CodeFile is null)
		{
            await DisplayAlert("Please, pick a code file", "You must pick a code file in order to execute MIPS!", "OK");
			return;
        }

        var stream = await CodeFile.OpenReadAsync();

        var instructions = await InstructionParser.ParseAsync(stream);

		var pipeline = new Pipeline(_mipsConfig, instructions);

		pipeline.Run();
    }
}

