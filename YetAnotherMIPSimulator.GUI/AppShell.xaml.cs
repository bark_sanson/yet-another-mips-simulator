﻿using YetAnotherMIPSimulator.MIPS.Config;

namespace YetAnotherMIPSimulator.GUI;

public partial class AppShell : Shell
{
	public AppShell()
	{
		InitializeComponent();

		Routing.RegisterRoute("MIPS-configuration", typeof(MIPSConfigPage));
    }
}
