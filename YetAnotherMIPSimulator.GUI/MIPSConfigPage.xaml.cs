using YetAnotherMIPSimulator.MIPS.Config;

namespace YetAnotherMIPSimulator.GUI;

[QueryProperty(nameof(Config), "config")]
public partial class MIPSConfigPage : ContentPage
{
    private MIPSConfig _mipsConfig;
	public MIPSConfig Config 
	{
        set
        {
            _mipsConfig = value;

            UpdateUI();
        }
    }

    public MIPSConfigPage()
	{
		InitializeComponent();

        saveBtn.Clicked += async (sender, obj) =>
        {
            _mipsConfig.RegisterNumber = int.Parse(regNum.Text);
            _mipsConfig.MaxInstructionMemSize = int.Parse(insSize.Text);
            _mipsConfig.MaxDataMemSize = int.Parse(memSize.Text);

            await Shell.Current.GoToAsync("..",
            new Dictionary<string, object>
            {
                { "config", _mipsConfig }
            });
        };
    }

    private void UpdateUI()
    {
        regNum.Text = _mipsConfig.RegisterNumber.ToString();
        insSize.Text = _mipsConfig.MaxInstructionMemSize.ToString();
        memSize.Text = _mipsConfig.MaxDataMemSize.ToString();
    }
}