﻿using System.Diagnostics;
using YetAnotherMIPSimulator.MIPS.Config;
using YetAnotherMIPSimulator.MIPS.Memories;
using YetAnotherMIPSimulator.MIPS.Registers;
using YetAnotherMIPSimulator.MIPS.Reports;
using YetAnotherMIPSimulator.MIPS.Stages;

namespace YetAnotherMIPSimulator.MIPS;
public class Pipeline 
{
	private int NUM_STAGES = 5;

    private int DECODE_POS = 0;
    private int EXEC_POS = 1;
    private int MEM_POS = 2;
    private int WB_POS = 3;

    private int _cycle;

    private readonly ProgramCounter _pc;
    private readonly InstructionMemory _instructionMemory;
    private readonly DataMemory _dataMemory;
    private readonly RegisterFile _registerFile;

	private readonly InstructionFetchStage _fetch;
	private readonly InstructionDecodeStage _decode;
	private readonly ExecutionStage _execution;
	private readonly MemoryStage _memory;
	private readonly WriteBackStage _writeback;

	private readonly FetchDecodePipelineRegister _ifid;
	private readonly DecodeExecutionPipelineRegister _idex;
	private readonly ExecutionMemoryPipelineRegister _exmem;
	private readonly MemoryWritebackPipelineRegister _memwb;

	private readonly ForwardingUnit _forwardingUnit;

	private IList<Instruction> _left;
    private List<Queue<Instruction>> _instructionsAtStage;

    public Pipeline(
		MIPSConfig mipsConfig,
		IEnumerable<Instruction> instructions)
	{
		_pc = new();

		_instructionMemory = new(mipsConfig.MaxInstructionMemSize, instructions);
		_dataMemory = new(mipsConfig.MaxDataMemSize);

		_registerFile = new(mipsConfig.RegisterNumber);

		_ifid = new();
		_idex = new();
		_exmem = new();
		_memwb = new();

        _forwardingUnit = new(_idex, _exmem, _memwb);

        _left = instructions.ToList();

        // Fetch doesn't need a queue, thus NUM_STAGES - 1 queues are created
        _instructionsAtStage = 
			Enumerable.Repeat(new Queue<Instruction>(), NUM_STAGES - 1).ToList();

		_fetch = new(_pc, _instructionMemory, _ifid, _instructionsAtStage[DECODE_POS]);
		_decode = new(_pc, _registerFile, _ifid, _idex, _instructionsAtStage[DECODE_POS], _instructionsAtStage[EXEC_POS]);
		_execution = new(_idex, _exmem, _memwb, _instructionsAtStage[EXEC_POS], _instructionsAtStage[MEM_POS]);
		_memory = new(_dataMemory, _exmem, _memwb, _instructionsAtStage[MEM_POS], _instructionsAtStage[WB_POS]);
		_writeback = new(_registerFile, _memwb, _instructionsAtStage[WB_POS], _left);

        _cycle = 0;
	}

    public void Run()
	{
		var exReport = new ExecutionReport();
		while(_left.Any())
		{
            _cycle++;

            _fetch.Execute();
            _decode.Execute();
            _execution.Execute();
            _memory.Execute();
            _writeback.Execute();

            _forwardingUnit.Forward();
        }
    }
}
