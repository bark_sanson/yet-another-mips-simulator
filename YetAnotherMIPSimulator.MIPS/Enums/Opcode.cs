﻿namespace YetAnotherMIPSimulator.MIPS.Enums;

public enum Opcode
{
    ADD,
    SUB,
    LW,
    SW,
}
