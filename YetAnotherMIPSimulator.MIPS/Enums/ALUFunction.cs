﻿namespace YetAnotherMIPSimulator.MIPS.Enums;

public enum ALUFunction
{
    PASS_THROUGH,
    ADD,
    SUB,
}
