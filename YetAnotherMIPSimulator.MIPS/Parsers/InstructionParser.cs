﻿using YetAnotherMIPSimulator.MIPS.Enums;

namespace YetAnotherMIPSimulator.MIPS.Parsers;

public static class InstructionParser
{

    public static async Task<IEnumerable<Instruction>> ParseAsync(Stream codeFile)
    {

        var instructions = new List<Instruction>();
        using (var reader = new StreamReader(codeFile))
        {
            var str = await reader.ReadLineAsync();
            int linia = 1;

            while (str is not null)
            {
                if (str.Equals(string.Empty))
                {
                    str = await reader.ReadLineAsync();
                    continue;
                }

                var instrString = str.Split(' ', StringSplitOptions.TrimEntries);
                var opcodeString = instrString[0];

                var opcode = StringToOpcode(opcodeString);

                var operandsString = instrString[1];
                var operands = operandsString.Split(',', StringSplitOptions.TrimEntries);

                var src = opcode switch
                {
                    Opcode.LW => new List<string> { operands[1] },
                    Opcode.SW => new List<string> { operands[0] },
                    _ => new List<string> { operands[1], operands[2] }
                };

                var dst = opcode switch
                {
                    Opcode.LW => operands[0],
                    Opcode.SW => operands[1],
                    _ => operands[0]
                };

                var instruction = new Instruction(opcode, src, dst, linia);

                instructions.Add(instruction);

                str = await reader.ReadLineAsync();
                linia++;
            }

        }

        return instructions;
    }

    private static Opcode StringToOpcode(string opcodeString) => opcodeString switch
    {
        "ADD" => Opcode.ADD,
        "SUB" => Opcode.SUB,
        "LW" => Opcode.LW,
        "SW" => Opcode.SW,
        _ => throw new ArgumentException($"Opcode {opcodeString} doesn't exist")
    };

}
