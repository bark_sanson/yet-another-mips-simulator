﻿using YetAnotherMIPSimulator.MIPS.Enums;

namespace YetAnotherMIPSimulator.MIPS;

internal static class ALU
{
    public static int Compute(ALUFunction function, int data1, int data2)
        => function switch
        {
            ALUFunction.ADD => Add(data1, data2),
            ALUFunction.SUB => Sub(data1, data2),
            ALUFunction.PASS_THROUGH => data2,
            _ => throw new ArgumentException("Operation not accepted by ALU")
        };
        
    private static int Add(int x, int y) => x + y;
    private static int Sub(int x, int y) => x - y;
}
