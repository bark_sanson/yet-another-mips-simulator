﻿using YetAnotherMIPSimulator.MIPS.Enums;

namespace YetAnotherMIPSimulator.MIPS;

public class Instruction
{
    public Opcode Opcode { init; get; }
    public IEnumerable<string> Src { init; get; }
    public string Dst { init; get; }
    public int Line { init; get; }

    public Instruction(
        Opcode opcode,
        IEnumerable<string> src,
        string dst,
        int line)
    {
        Opcode = opcode;
        Src = src;
        Dst = dst;
        Line = line;
    }
}
