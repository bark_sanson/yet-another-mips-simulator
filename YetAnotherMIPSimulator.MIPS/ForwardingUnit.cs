﻿using YetAnotherMIPSimulator.MIPS.Registers;

namespace YetAnotherMIPSimulator.MIPS;

internal class ForwardingUnit
{
    private readonly DecodeExecutionPipelineRegister _idex;
    private readonly ExecutionMemoryPipelineRegister _exmem;
    private readonly MemoryWritebackPipelineRegister _memwb;

    public ForwardingUnit(
        DecodeExecutionPipelineRegister idex, 
        ExecutionMemoryPipelineRegister exmem, 
        MemoryWritebackPipelineRegister memwb)
    {
        _idex = idex;
        _exmem = exmem;
        _memwb = memwb;
    }

    public bool Forward()
    {
        var forwarded = false;
        // EX hazard
        if (_exmem.RegWrite && !_exmem.RegisterRd.Equals(string.Empty)
            && _exmem.RegisterRd.Equals(_idex.RegisterRs))
        {
            // If it's a store
            if (_idex.MemWrite)
            {
                _idex.MemoryWriteData = _exmem.ALUResult;
            }
            else
            {
                // ForwardA = 10
                _idex.Data1 = _exmem.ALUResult;
            }
        }

        if (_exmem.RegWrite && !_exmem.RegisterRd.Equals(string.Empty)
            && _exmem.RegisterRd.Equals(_idex.RegisterRt))
        {
            // ForwardB = 10
            _idex.Data2 = _exmem.ALUResult;
        }

        if (_memwb.RegWrite && !_memwb.RegisterRd.Equals(string.Empty)
            && !(_exmem.RegWrite && !_exmem.RegisterRd.Equals(string.Empty)
                && _exmem.RegisterRd.Equals(_idex.RegisterRs))
            && _memwb.RegisterRd.Equals(_idex.RegisterRs))
        {
            // ForwardA = 01
            _idex.Data1 = _memwb.MemoryOutput;
        }

        if (_memwb.RegWrite && !_memwb.RegisterRd.Equals(string.Empty)
            && !(_exmem.RegWrite && !_exmem.RegisterRd.Equals(string.Empty)
                && _exmem.RegisterRd.Equals(_idex.RegisterRt))
            && _memwb.RegisterRd.Equals(_idex.RegisterRt))
        {
            // ForwardB = 01
            _idex.Data2 = _memwb.MemoryOutput;
        }

        return forwarded;
    }
}
