﻿namespace YetAnotherMIPSimulator.MIPS.Stages;

internal interface IPipeStage
{
    void Execute();
}
