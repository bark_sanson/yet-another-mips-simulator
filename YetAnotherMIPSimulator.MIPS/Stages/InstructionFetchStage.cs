﻿using YetAnotherMIPSimulator.MIPS.Memories;
using YetAnotherMIPSimulator.MIPS.Registers;

namespace YetAnotherMIPSimulator.MIPS.Stages;

internal class InstructionFetchStage : IPipeStage
{
    private readonly ProgramCounter _pc;
    private readonly InstructionMemory _instructionMemory;
    private readonly FetchDecodePipelineRegister _ifid;
    private readonly Queue<Instruction> _nextStageInstructions;

    public InstructionFetchStage(
        ProgramCounter pc,
        InstructionMemory instructionMemory,
        FetchDecodePipelineRegister ifid,
        Queue<Instruction> nextStageInstructions)
    {
        _pc = pc;
        _instructionMemory = instructionMemory;
        _ifid = ifid;
        _nextStageInstructions = nextStageInstructions;
    }

    public void Execute()
    {
        if (_pc.Counter > (_instructionMemory.Instructions.Count - 1) * 4) return;

        var instruction = _instructionMemory.Load(_pc.Counter);

        _pc.Increment();

        _ifid.Instruction = instruction;
        _nextStageInstructions.Enqueue(instruction);
    }
}
