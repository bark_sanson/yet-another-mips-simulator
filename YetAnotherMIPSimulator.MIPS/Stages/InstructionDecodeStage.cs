﻿using YetAnotherMIPSimulator.MIPS.Enums;
using YetAnotherMIPSimulator.MIPS.Registers;

namespace YetAnotherMIPSimulator.MIPS.Stages;

internal class InstructionDecodeStage : IPipeStage
{
    private readonly ProgramCounter _pc;
    private readonly RegisterFile _registerFile;
    private readonly FetchDecodePipelineRegister _ifid;
    private readonly DecodeExecutionPipelineRegister _idex;
    private readonly Queue<Instruction> _selfInstructions;
    private readonly Queue<Instruction> _nextStageInstructions;

    public InstructionDecodeStage(
        ProgramCounter pc,
        RegisterFile registerFile, 
        FetchDecodePipelineRegister ifid, 
        DecodeExecutionPipelineRegister idex,
        Queue<Instruction> selfInstructions,
        Queue<Instruction> nextStageInstructions)
    {
        _registerFile = registerFile;
        _ifid = ifid;
        _idex = idex;
        _selfInstructions = selfInstructions;
        _nextStageInstructions = nextStageInstructions;
        _pc = pc;
    }

    public void Execute()
    {
        if (_selfInstructions.Count <= 0) return;
        var instr = _selfInstructions.Dequeue();

        // Stall condition
        if (_idex.MemRead
            && _ifid.Instruction.Src.Any(s => s.Equals(_idex.RegisterRt)))
        {
            _idex.MemWrite = false;
            _idex.MemRead = false;
            _idex.RegWrite = false;
            _ifid.Stall = true;

            _pc.Decrement();
        } 
        else // If stall is not needed, can proceed as normal
        {
            var instruction = _ifid.Instruction;

            var aluFunction = OpcodeToALUFunction(instruction.Opcode);

            if (instruction.Opcode is Opcode.LW)
            {
                _idex.MemWrite = false;
                _idex.MemRead = true;
                _idex.MemToReg = true;

                _idex.RegWrite = true;
                _idex.RegisterRd = instruction.Dst;
                _idex.RegisterRt = instruction.Dst;
                _idex.RegisterRs = string.Empty;

                _idex.Function = aluFunction;
                _idex.Data1 = default;
                _idex.Data2 = int.Parse(instruction.Src.First());

                _nextStageInstructions.Enqueue(instr);

                return;
            }

            if (instruction.Opcode is Opcode.SW)
            {
                var registerToRead = instruction.Src.First();

                _idex.MemWrite = true;
                _idex.MemRead = false;
                _idex.MemToReg = false;

                _idex.RegWrite = false;
                _idex.RegisterRd = string.Empty;
                _idex.RegisterRs = registerToRead;

                _idex.Function = aluFunction;
                _idex.Data1 = default;
                _idex.Data2 = int.Parse(instruction.Dst);

                _idex.MemoryWriteData = _registerFile.Request(instruction.Src.First());

                _nextStageInstructions.Enqueue(instr);

                return;
            }

            var src = instruction.Src;

            var values = _registerFile.Request(src).ToArray();

            _idex.MemWrite = false;
            _idex.MemRead = false;
            _idex.MemToReg = false;

            _idex.Function = aluFunction;
            _idex.Data1 = values[0];
            _idex.Data2 = values[1];

            _idex.RegWrite = true;
            _idex.RegisterRs = src.First();
            _idex.RegisterRt = src.ToArray()[1];
            _idex.RegisterRd = instruction.Dst;

            _nextStageInstructions.Enqueue(instr);
        }

        
    }

    private ALUFunction OpcodeToALUFunction(Opcode opcode) => opcode switch
    {
        Opcode.LW or Opcode.SW => ALUFunction.PASS_THROUGH,
        Opcode.ADD => ALUFunction.ADD,
        Opcode.SUB => ALUFunction.SUB,
        _ => throw new ArgumentException($"Opcode {opcode.ToString()} doesn't exist")
    };

}
