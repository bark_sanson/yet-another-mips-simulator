﻿using YetAnotherMIPSimulator.MIPS.Registers;

namespace YetAnotherMIPSimulator.MIPS.Stages;

internal class WriteBackStage : IPipeStage
{
    private readonly RegisterFile _registerFile;
    private readonly MemoryWritebackPipelineRegister _memwb;
    private readonly Queue<Instruction> _instructions;
    private readonly IList<Instruction> _left;

    public WriteBackStage(
        RegisterFile registerFile,
        MemoryWritebackPipelineRegister memwb,
        Queue<Instruction> instructions,
        IList<Instruction> left)
    {
        _registerFile = registerFile;
        _memwb = memwb;
        _instructions = instructions;
        _left = left;
    }

    public void Execute()
    {
        if (_instructions.Count <= 0) return;

        if (_memwb.RegWrite)
        {
            if (_memwb.MemToReg)
            {
                _registerFile.Store(_memwb.RegisterRd, _memwb.MemoryOutput);
            }
            else
            {
                _registerFile.Store(_memwb.RegisterRd, _memwb.ALUResult);
            }
        }


        var done = _instructions.Dequeue();
        _left.Remove(done);
    }
}
