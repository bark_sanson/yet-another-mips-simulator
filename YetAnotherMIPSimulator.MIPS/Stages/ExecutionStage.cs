﻿using YetAnotherMIPSimulator.MIPS.Registers;

namespace YetAnotherMIPSimulator.MIPS.Stages;
internal class ExecutionStage : IPipeStage
{
    private readonly DecodeExecutionPipelineRegister _idex;
    private readonly ExecutionMemoryPipelineRegister _exmem;
    private readonly MemoryWritebackPipelineRegister _memwb;
    private readonly Queue<Instruction> _selfInstructions;
    private readonly Queue<Instruction> _nextStageInstructions;

    public ExecutionStage(
        DecodeExecutionPipelineRegister idex,
        ExecutionMemoryPipelineRegister exmem,
        MemoryWritebackPipelineRegister memwb,
        Queue<Instruction> selfInstructions,
        Queue<Instruction> nextStageInstructions)
    {
        _exmem = exmem;
        _idex = idex;
        _memwb = memwb;
        _selfInstructions = selfInstructions;
        _nextStageInstructions = nextStageInstructions;
    }

    public void Execute()
    {
        if (_selfInstructions.Count <= 0) return;

        _exmem.RegWrite = _idex.RegWrite;
        _exmem.MemWrite = _idex.MemWrite;
        _exmem.MemRead = _idex.MemRead;
        _exmem.MemoryWriteData = _idex.MemoryWriteData;
        _exmem.RegisterRd = _idex.RegisterRd;
        _exmem.MemToReg = _idex.MemToReg;

        var op = _idex.Function;
        var data1 = _idex.Data1;
        var data2 = _idex.Data2;

        _exmem.ALUResult = ALU.Compute(op, data1, data2);

        var instr = _selfInstructions.Dequeue();
        _nextStageInstructions.Enqueue(instr);
    }
}
