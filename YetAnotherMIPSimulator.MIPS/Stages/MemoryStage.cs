﻿using YetAnotherMIPSimulator.MIPS.Memories;
using YetAnotherMIPSimulator.MIPS.Registers;

namespace YetAnotherMIPSimulator.MIPS.Stages;

internal class MemoryStage : IPipeStage
{
    private readonly DataMemory _dataMemory;
    private readonly ExecutionMemoryPipelineRegister _exmem;
    private readonly MemoryWritebackPipelineRegister _memwb;
    private readonly Queue<Instruction> _selfInstructions;
    private readonly Queue<Instruction> _nextStageInstructions;

    public MemoryStage(
        DataMemory dataMemory,
        ExecutionMemoryPipelineRegister exmem,
        MemoryWritebackPipelineRegister memwb,
        Queue<Instruction> instructions,
        Queue<Instruction> nextStageInstructions)
    {
        _dataMemory = dataMemory;
        _exmem = exmem;
        _memwb = memwb;
        _selfInstructions = instructions;
        _nextStageInstructions = nextStageInstructions;
    }

    public void Execute()
    {
        if (_selfInstructions.Count <= 0) return;

        _memwb.RegisterRd = _exmem.RegisterRd;
        _memwb.RegWrite = _exmem.RegWrite;

        int value = default;
        if (_exmem.MemWrite && !_exmem.MemRead)
        {
            _dataMemory.Store(_exmem.ALUResult, _exmem.MemoryWriteData);
        } else if (!_exmem.MemWrite && _exmem.MemRead)
        {
            value = _dataMemory.Load(_exmem.ALUResult);
        }

        _memwb.ALUResult = _exmem.ALUResult;
        _memwb.MemoryOutput = value;
        _memwb.MemToReg = _exmem.MemToReg;

        var instr = _selfInstructions.Dequeue();
        _nextStageInstructions.Enqueue(instr);
    }
}