﻿namespace YetAnotherMIPSimulator.MIPS.Memories;

internal class DataMemory : IMemory<int, int>
{
    private Dictionary<int, int> _mem;
    private int _maxSize;

    public DataMemory(int maxSize)
    {
        _maxSize = maxSize;
        _mem = new()
        {
            { 32, 5 },
            { 16, 5 }
        };
    }

    public int Load(int memoryPosition)
    {
        if (memoryPosition < 0 || memoryPosition > _maxSize)
            throw new ArgumentOutOfRangeException(
                $"Segmentation fault: memory position must be between 0 and {_maxSize}");


        if (!_mem.ContainsKey(memoryPosition))
            return 0;

        return _mem[memoryPosition];
    }

    public void Store(int memoryPosition, int value)
    {
        if (memoryPosition < 0 || memoryPosition > _maxSize)
            throw new ArgumentOutOfRangeException($"Memory position must be between 0 and {_maxSize}");

        _mem[memoryPosition] = value;
    }
}
