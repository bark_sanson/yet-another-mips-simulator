﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace YetAnotherMIPSimulator.MIPS.Memories;

internal class InstructionMemory : IMemory<int, Instruction>
{
    public Dictionary<int, Instruction> Instructions { get; }
    private readonly int _maxSize;

    public InstructionMemory(int maxSize, IEnumerable<Instruction> instructions)
    {
        if (maxSize < instructions.Count())
            _maxSize = instructions.Count();
        else
            _maxSize = maxSize;

        Instructions = instructions
            .Select((ins, i) => (ins, i))
            .ToDictionary(pair => pair.i * 4, pair => pair.ins);
    }

    public Instruction Load(int memoryPosition)
    {
        if (!Instructions.ContainsKey(memoryPosition))
            throw new ArgumentOutOfRangeException(
                $"Memory position {memoryPosition} doesn't have an instruction stored");

        return Instructions[memoryPosition];
    }

    public void Store(int memoryPosition, Instruction value)
    {
        if (memoryPosition < 0 || memoryPosition > _maxSize)
            throw new ArgumentOutOfRangeException($"Memory position must be between 0 and {_maxSize}");

        Instructions.Add(memoryPosition, value);
    }
}
