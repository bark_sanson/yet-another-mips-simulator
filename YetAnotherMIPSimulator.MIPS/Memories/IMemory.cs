﻿namespace YetAnotherMIPSimulator.MIPS.Memories;

public interface IMemory<K, V>
{
    void Store(K memoryPosition, V value);
    V Load(K memoryPosition);
}
