﻿namespace YetAnotherMIPSimulator.MIPS.Reports;

public class CycleReport
{
    public int CycleNum { get; set; }
    public Instruction InstructionFinished { get; set; }
}
