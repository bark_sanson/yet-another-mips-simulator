﻿namespace YetAnotherMIPSimulator.MIPS.Reports;

internal class ExecutionReport
{
    private readonly IList<CycleReport> _cycleStates = new List<CycleReport>();

    public void AddCycle(int cycle, Instruction finished)
    {
        var state = new CycleReport
        {
            CycleNum = cycle,
            InstructionFinished = finished
        };

        _cycleStates.Add(state);
    }
}
