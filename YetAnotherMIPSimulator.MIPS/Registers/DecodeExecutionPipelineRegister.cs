﻿using YetAnotherMIPSimulator.MIPS.Enums;

namespace YetAnotherMIPSimulator.MIPS.Registers; 
internal class DecodeExecutionPipelineRegister 
{
    public ALUFunction Function { get; set; }
    public int Data1 { get; set; }
    public int Data2 { get; set; }
    public string RegisterRs { get; set; } = string.Empty;
    public string RegisterRt { get; set; } = string.Empty;

    // MEM stage flags and data
    public int MemoryWriteData { get; set; }
    public bool MemRead { get; set; }
    public bool MemWrite { get; set; }

    // Flags and data for WB
    public bool MemToReg { get; set; }
    public bool RegWrite { get; set; }
    public string RegisterRd { get; set; } = string.Empty;

}
