﻿namespace YetAnotherMIPSimulator.MIPS.Registers;

internal class ProgramCounter
{
    private int _counter;

    public int Counter
    {
        get => _counter;
    }

    public void Increment()
    {
        _counter += 4;
    }

    public void Decrement()
    {
        _counter -= 4;
    }
}
