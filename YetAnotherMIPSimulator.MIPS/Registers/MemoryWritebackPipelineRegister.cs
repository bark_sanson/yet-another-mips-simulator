﻿namespace YetAnotherMIPSimulator.MIPS.Registers; 
internal class MemoryWritebackPipelineRegister 
{
    public int MemoryOutput { get; set; }
    public int ALUResult { get; internal set; }

    // Flags and data for WB
    public bool MemToReg { get; set; }
    public bool RegWrite { get; internal set; }
    public string RegisterRd { get; set; } = string.Empty;
}
