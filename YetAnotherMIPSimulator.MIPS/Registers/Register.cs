﻿namespace YetAnotherMIPSimulator.MIPS.Registers;

internal class Register
{
    public string Name { init; get; }

    public int Value { get; set; }

    public Register(string name)
    {
        Name = name;
        Value = 0;
    }
}
