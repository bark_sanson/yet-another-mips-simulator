﻿namespace YetAnotherMIPSimulator.MIPS.Registers;

internal class RegisterFile
{
    private IList<Register> _registers;

    public RegisterFile(int numberOfRegisters)
    {
        _registers = Enumerable.Range(1, numberOfRegisters)
            .Select(i => new Register($"r{i}"))
            .ToList();
    }

    public IEnumerable<int> Request(IEnumerable<string> registers)
    {
        if (registers.Count() > 2)
            throw new ArgumentOutOfRangeException("Too many registers requested");

        return registers
            .Select(reg => Request(reg));
    }

    public int Request(string r1)
    {
        var reg1 = _registers.FirstOrDefault(r => r.Name == r1);

        if (reg1 is null)
            throw new ArgumentException("The register is not in the register file");

        return reg1.Value;
    }

    public void Store(string reg, int value)
    {
        var register = _registers.FirstOrDefault(r => r.Name == reg);

        if (register is null)
            throw new ArgumentException("The register name is not in the register file");

        register.Value = value;
    }
}
