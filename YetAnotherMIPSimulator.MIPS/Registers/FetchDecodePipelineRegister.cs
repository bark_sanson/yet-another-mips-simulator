﻿namespace YetAnotherMIPSimulator.MIPS.Registers;

internal class FetchDecodePipelineRegister
{
    public int PC { get; set; }
    public Instruction Instruction { get; set; }
    public bool Stall { get; set; }
}
