﻿namespace YetAnotherMIPSimulator.MIPS.Registers;

internal class ExecutionMemoryPipelineRegister
{
    // EX stage data
    public int ALUResult { get; set; }

    // MEM stage flags and data
    public bool MemRead { get; set; }
    public bool MemWrite { get; set; }
    public int MemoryWriteData { get; set; }

    // Flags and data for WB
    public bool RegWrite { get; set; }
    public bool MemToReg { get; set; }
    public string RegisterRd { get; set; } = string.Empty;
}
