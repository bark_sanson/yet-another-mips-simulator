﻿namespace YetAnotherMIPSimulator.MIPS.Config;

public class MIPSConfig
{
    public int RegisterNumber { get; set; } = 32;
    public int MaxInstructionMemSize { get; set; } = 100;
    public int MaxDataMemSize { get; set; } = 100;
}
